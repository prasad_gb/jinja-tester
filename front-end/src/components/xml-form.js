import React, { useState } from "react";
import {
  Button,
  TextField,
  Checkbox,
  FormControlLabel,
} from "@material-ui/core";
import TextEditor from "./text-editor";
import { postCall } from "../util/restCall";
import ErrorModal from "./error-modal";
import SimpleRadio from "./../common/simple-radio";

const initialFormState = {
  selectedPreset: "escaped",
  selectedNumber: "numbered",
  workflow: "",
  movie: "",
  ignore: "",
  other: "",
  apiError: null,
};

const XMLForm = (props) => {
  const { sendXMLData, clearResult, handleLoading } = props;
  const [json, setjson] = useState({ value: "" });
  const [jinja, setjinja] = useState({ value: "" });
  const [error, setError] = useState(null);
  const [modalOpen, setmodalOpen] = useState(false);
  const [formValue, setformValue] = useState({ ...initialFormState });
  const [unescaped, setUnescaped] = useState(false);

  const handleJsonChange = (editor, data, value) => {
    setjson({ editor, data, value });
  };
  const handleJinjaChange = (editor, data, value) => {
    setjinja({ editor, data, value });
  };
  const handleFormSubmit = () => {
    const request_info = {
      template: jinja["value"],
      dynamic: json["value"],
      workflow: " ",
      movie: " ",
      other: " ",
      ignore: " ",
      // escaped: unescaped ? "unescaped" : "escaped",
      escaped: "escaped",
      numbering: "",
      input_type: "json",
    };
    const error = handleValidate();
    setError(error);
    if (error) return;
    handleLoading(true);
    postCall(
      "http://localhost:8080/convert",
      { request_info },
      {
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        },
      }
    )
      .then(({ data }) => {
        sendXMLData(data);
      })
      .catch(({ response }) => {
        //Setting the api error to state and opening the model window
        const form = formValue;
        console.log(response);
        const { data } = response;
        const errObj = {
          status: response.status,
          message: data.message,
        };
        form["apiError"] = errObj;
        setformValue({ ...form });
        setmodalOpen(true);
        //Clearing the previous error if there is any
        clearResult();
      })
      .finally(() => {
        handleLoading(false);
      });
  };
  const handleValidate = () => {
    const error = {};
    if (json["value"].trim() === "")
      error["json"] = "Dynamic preset data value cannot empty";
    if (jinja["value"].trim() === "") error["jinja"] = "Template cannot empty";
    return Object.keys(error).length === 0 ? null : error;
  };
  const handleModal = () => {
    setmodalOpen(!modalOpen);
  };
  const handleInputClear = () => {
    setjson({ value: "" });
    setjinja({ value: "" });
    setformValue({ ...initialFormState });
    setError(null);
    clearResult();
  };
  const handlePresetEscape = (event) => {
    setUnescaped(event.target.checked);
  };
  return (
    <>
      <div className="grid-container grid-mainContent">
        <div>
          <div className="formHeadingSection">
            <h5>Template</h5>
          </div>
          <TextEditor
            value={jinja["value"]}
            className="codeEditor"
            options={{
              mode: "jinja2",
              theme: "dracula",
              lineNumbers: true,
              lineWrapping: true,
            }}
            onBeforeChange={(editor, data, value) => {
              handleJinjaChange(editor, data, value);
            }}
          />
          {error && <p className="form-error">{error.jinja}</p>}
        </div>
        <div>
          <div className="formHeadingSection">
            <h5>Dynamic Preset Data</h5>
            {/* <FormControlLabel
              control={
                <Checkbox
                  checked={unescaped}
                  onChange={handlePresetEscape}
                  name="upescaped"
                />
              }
              label="To Escape"
            /> */}
          </div>
          <TextEditor
            value={json["value"]}
            className="codeEditor"
            options={{
              mode: "jsx",
              theme: "dracula",
              lineNumbers: true,
              lineWrapping: true,
            }}
            onBeforeChange={(editor, data, value) => {
              handleJsonChange(editor, data, value);
            }}
          />
          {error && <p className="form-error">{error.json}</p>}
        </div>
      </div>
      <div className="grid-container grid-secContent">
        <div className="btn-wrapper">
          <Button
            style={{ background: "green", fontFamily: "montserrat-semibold" }}
            variant="contained"
            color="primary"
            onClick={handleFormSubmit}
            className="cta-btn"
          >
            Generate
          </Button>
          <Button
            style={{ background: "red", fontFamily: "montserrat-semibold" }}
            variant="contained"
            color="secondary"
            onClick={handleInputClear}
            className="cta-btn"
          >
            Clear
          </Button>
          <ErrorModal
            open={modalOpen}
            setOpen={handleModal}
            error={formValue["apiError"]}
          />
        </div>
      </div>
    </>
  );
};

export default XMLForm;
