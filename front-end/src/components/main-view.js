import React, { useState } from "react";
import { animated, useSpring } from "react-spring";
import { postCall } from "./../util/restCall";
import XMLForm from "./xml-form";
import ResultView from "./result-view";
import { act } from "react-dom/test-utils";

export default function MainView(props) {
  const { handleLoading: setLoading } = props;
  const [xmlValue, setXMLValue] = useState({});
  const [toShowResult, settoShowResult] = useState(false);
  const handleXMLData = (data) => {
    setXMLValue(data);
    settoShowResult(true);
  };
  const handleClearXMLData = () => {
    setXMLValue({});
    settoShowResult(false);
  };
  const handleDownload = () => {
    const fileValue = xmlValue["template_output"];
    // download stuff
    var fileName = `output_${new Date().toUTCString()}.xml`;
    var buffer = fileValue;
    var blob = new Blob([buffer], {
      type: "text/xml;charset=utf8;",
    });
    var link = document.createElement("a");

    if (link.download !== undefined) {
      // feature detection
      // Browsers that support HTML5 download attribute
      link.setAttribute("href", window.URL.createObjectURL(blob));
      link.setAttribute("download", fileName);
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    } else {
      alert("File download only works in Chrome, Firefox, and Opera.");
    }
  };
  const propsSpring = useSpring({
    opacity: 1,
    from: { opacity: 0 },
  });
  return (
    <>
      <XMLForm
        sendXMLData={handleXMLData}
        clearResult={handleClearXMLData}
        handleLoading={setLoading}
      />
      {toShowResult && (
        <animated.div style={propsSpring}>
          <ResultView data={xmlValue} xmlDownload={handleDownload} />
        </animated.div>
      )}
    </>
  );
}
