import React from "react";
import { Controlled as CodeMirror } from "react-codemirror2";
require("codemirror/mode/xml/xml");
require("codemirror/mode/jinja2/jinja2");
require("codemirror/mode/jsx/jsx");

const TextEditor = (props) => {
  return <CodeMirror {...props} />;
};

export default TextEditor;
