import React, { useRef, Fragment } from "react";
import XMLWarning from "../util/xml.warning";
import TextEditor from "./text-editor";
import { Button } from "@material-ui/core";
import GetAppRoundedIcon from "@material-ui/icons/GetAppRounded";

const ResultView = (props) => {
  const { data, xmlDownload } = props;
  const warningRef = useRef(null);
  const emptyTagRef = useRef(null);
  const scrollTo = (ref) => {
    window.scrollTo({ top: ref.current.offsetTop, behavior: "smooth" });
  };
  return (
    <div className="resultWrap">
      <div className="resultHead">
        <h5 className="resultHead__heading">XML Output</h5>
        {data["has_warning"] && (
          <Button
            variant="outlined"
            color="primary"
            style={{ marginRight: "15px" }}
            className={"viewWarning"}
            onClick={() => scrollTo(warningRef)}
          >
            {data["warnings"].length} Warning
          </Button>
        )}
        {data["has_empty_tags"] && (
          <Button
            variant="outlined"
            style={{ marginRight: "15px" }}
            className={"viewEmptyTags"}
            onClick={() => scrollTo(emptyTagRef)}
          >
            {data["empty_tags"].length} Empty Tags
          </Button>
        )}
        <Button
          style={{ position: "absolute" }}
          variant="contained"
          color="primary"
          className={"result__download"}
          startIcon={<GetAppRoundedIcon />}
          onClick={xmlDownload}
        >
          Download
        </Button>
      </div>
      <TextEditor
        value={data.template_output}
        className="codeEditor resultViewer"
        options={{
          mode: "xml",
          theme: "dracula",
          lineNumbers: true,
          lineWrapping: true,
        }}
      />
      <span ref={warningRef}>
        {data.has_warning && <XMLWarning warnings={data.warnings} />}
      </span>
      {data.has_empty_tags && (
        <div ref={emptyTagRef} className="emptyTag">
          <h4 className="emptyTag__heading">Empty Tags</h4>
          <ul className="emptyTag__list">
            {data["empty_tags"].map((item, index) => {
              //Splitting string into empty tag message and empty value using special keyword $$$#
              const itemArray = item.split("$$$#");
              return (
                <li className="emptyTag__name" key={index}>
                  {itemArray[0]}
                  <span className="emptyTag__name--value">{itemArray[1]}</span>
                </li>
              );
            })}
          </ul>
        </div>
      )}
    </div>
  );
};

export default ResultView;
