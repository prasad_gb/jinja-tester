import React from "react";
import { AppBar, Toolbar } from "@material-ui/core";
// import jinjaLogo from "./../assets/images/jinja2-icon.svg";

const NavigationBar = () => {
  return (
    <AppBar position="static" style={{ background: "#005aff" }}>
      <Toolbar>
        {/* <img className="navBarLogo" src={jinjaLogo} /> */}
        <h3 className="mb-0">JINJA Tester</h3>
      </Toolbar>
    </AppBar>
  );
};

export default NavigationBar;
