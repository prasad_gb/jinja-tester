import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import LinearProgress from "@material-ui/core/LinearProgress";

const LinearLoader = (props) => {
  const { primaryColor, barColor } = props;
  const ColorLinearProgress = withStyles({
    colorPrimary: {
      backgroundColor: primaryColor || "#b2dfdb",
    },
    barColorPrimary: {
      backgroundColor: barColor || "#00695c",
    },
  })(LinearProgress);

  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
  }));
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <ColorLinearProgress className={classes.margin} />
    </div>
  );
};

export default LinearLoader;
