import React from "react";
import warningIcon from "./../assets/images/warning.svg";

const XMLWarning = (props) => {
  const { warnings } = props;
  return (
    <div className="warningWrap">
      <h2 className="warning__head">
        <img width="30px" src={warningIcon} alt="Warning Icon" />
        <span>Warning</span>
      </h2>
      {warnings &&
        warnings.map((singleWarnObject, index) => {
          const warningHead = singleWarnObject["warning_header"];
          let warningMessages = singleWarnObject["warning_message"];
          return (
            <div key={index} className="warningListWrap">
              <h5 className="warningListHeading">{warningHead}</h5>
              <ul className="warningList">
                {warningMessages &&
                  warningMessages.map((warningMessage, index) => {
                    //Splitting warning message into line number and message using special keyword.
                    const warningMessageArr = warningMessage.split("$$$###");
                    return (
                      <li key={index} className="warningList__item">
                        <span className="warningList__item--lineNo">
                          {warningMessageArr[0]}
                        </span>
                        {warningMessageArr[1]}
                      </li>
                    );
                  })}
              </ul>
            </div>
          );
        })}
    </div>
  );
};

export default XMLWarning;
