import axios from "axios";

export const getCall = (url, data) => {
  return axios.get(url, data);
};
export const postCall = (url, data) => {
  return axios.post(url, data);
};
