import React, { useState } from "react";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import "bootstrap/dist/css/bootstrap.min.css";

import "./App.css";
import MainView from "./components/main-view";
import NavigationBar from "./components/navigation-bar";
import LinearLoader from "./util/linear-loader";

const customTheme = createMuiTheme({
  typography: {
    fontFamily: `"montserrat-regular", "Helvetica", "Arial", sans-serif`,
    fontSize: 14,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
  },
});
function App() {
  const [isLoading, setLoading] = useState(false);
  const handleLoading = (loading = false) => {
    setLoading(loading);
  };
  return (
    <div className="App">
      <MuiThemeProvider theme={customTheme}>
        <NavigationBar />
        {isLoading && <LinearLoader />}
        <div className="mainContentWrap">
          <MainView handleLoading={handleLoading} />
        </div>
      </MuiThemeProvider>
    </div>
  );
}

export default App;
