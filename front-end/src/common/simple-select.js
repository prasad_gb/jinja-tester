import React from "react";
import Select from "react-select";

export default function SimpleSelect(props) {
  const { handleChange, value, menuItem } = props;
  return (
    <>
      <Select
        options={menuItem}
        clearable={true}
        name="selected-state"
        value={value}
        onChange={handleChange}
      />
    </>
  );
}
