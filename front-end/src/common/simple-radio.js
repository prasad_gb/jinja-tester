import React from "react";
import { Radio, RadioGroup, FormControlLabel } from "@material-ui/core";

const SimpleRadio = (props) => {
  const {
    option,
    labelPlacement,
    handleChange,
    colorScheme,
    name,
    value,
  } = props;
  return (
    <RadioGroup row value={value} onChange={handleChange}>
      {option.map((item) => {
        return (
          <FormControlLabel
            className="radio-class"
            key={item.value}
            value={item.value}
            control={<Radio color={colorScheme || "primary"} />}
            label={item.label}
            labelPlacement={labelPlacement}
            name={name}
          />
        );
      })}
    </RadioGroup>
  );
};
export default SimpleRadio;
