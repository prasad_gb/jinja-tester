from flask import Flask, request, render_template, Response
from flask_cors import CORS
import logging
import logging.handlers
import configuration as config
from jinja2 import Environment
from flask import jsonify
from jinjamethods import *
import json

app = Flask(__name__)
CORS(app)

@app.route("/")
def helloWorld():
    return render_template('index.html')


@app.route('/convert', methods=['POST'])
def convert():
    jinja2_env = Environment()
    has_warning = False
    json_request = request.get_json(force=True)

    if request.method == "POST":
        error_jinja = check_valid_template(jinja2_env, json_request)
        if error_jinja:
            apiObj = {
                'message': error_jinja
            }
            return apiObj, 400
        error_json ,json_data= check_valid_json(jinja2_env, json_request)

        if error_json:
            apiObj = {
                'message': error_json
            }
            return apiObj, 400

        result = success_data(jinja2_env,json_request,json_data)
        return result


if __name__ == "__main__":
    # Set up logging
    app.logger.setLevel(logging.__getattribute__(config.LOGGING_LEVEL))
    file_handler = logging.handlers.RotatingFileHandler(
        filename=config.LOGGING_LOCATION, maxBytes=10*1024*1024, backupCount=5)
    file_handler.setFormatter(logging.Formatter(config.LOGGING_FORMAT))
    file_handler.setLevel(logging.__getattribute__(config.LOGGING_LEVEL))
    app.logger.addHandler(file_handler)
    # app.run(host="0.0.0.0")
    app.run(host=config.HOST, port=config.PORT)
