{% set providerassetId = DYNAMIC_PRESET_DATA['descriptiveMetadata']['psa.orig.providerAssetId'] | replace("_","") | default("") %}
 {%- if DYNAMIC_PRESET_DATA['calculatedData']['durationInMinutes'] is defined  -%}
{% set string_conv = DYNAMIC_PRESET_DATA['calculatedData']['durationInMinutes'] | string %}
{%- if "." in string_conv and string_conv.split('.')[1] | int > 0 %}
{% set adjusted = string_conv.split('.')[0] | int + 1 %}
{%- else -%}
{% set adjusted = string_conv %}
{%endif%}
{%endif%}
<?xml version="1.0" encoding="UTF-8"?>
 <package xmlns="http://apple.com/itunes/importer" version="tv5.2">
   {% if DYNAMIC_PRESET_DATA['descriptiveMetadata']['QCNotes'] is defined and DYNAMIC_PRESET_DATA['descriptiveMetadata']['QCNotes']|length %}
    <comments>{{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['QCNotes']}}</comments>
    {% else %}
    <comments/>
    {% endif %}
   <provider>Scripps</provider>
   <language>en-US</language>
   <video>
       <type>tv</type>
       <subtype>{{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['assetType']}}</subtype>
       <vendor_id>{{ providerassetId }}</vendor_id>
       {% if DYNAMIC_PRESET_DATA['descriptiveMetadata']['showcode'] is defined and DYNAMIC_PRESET_DATA['descriptiveMetadata']['showcode']|length %}
       <episode_production_number>{{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['showcode'] }}</episode_production_number>
       {% else %}
       <episode_production_number/>
       {% endif %}
       <original_spoken_locale>en-US</original_spoken_locale>
       <title>{{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['Title30'] | default("")}}</title>
       <studio_release_title>{{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['Title30'] | default("")}}</studio_release_title>
       {% if DYNAMIC_PRESET_DATA['descriptiveMetadata']['estSeriesId'] is defined and DYNAMIC_PRESET_DATA['descriptiveMetadata']['estSeriesId'] != ""%}
       <container_id>{{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['estSeriesId'] }}</container_id>
       {% else %}
       <container_id>{{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['Series ID'] | default("")}}</container_id>
       {% endif %}
       <container_position>{{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['Container Position Episode'] | default("")}}</container_position>
       {% if DYNAMIC_PRESET_DATA['descriptiveMetadata']['activationDate'] is defined or DYNAMIC_PRESET_DATA['descriptiveMetadata']['activationDate'] != ""  %}
       <release_date>{{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['activationDate'] }}</release_date>
       {% else %}
       <release_date>{{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['premierDateEST'][0:9]}}</release_date>
       {% endif %}
       <ratings>
           <rating system="us-tv">{{DYNAMIC_PRESET_DATA['descriptiveMetadata']['rating']}}</rating>
       </ratings>
       <copyright_cline>{{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['copyright'] | default("") }} Discovery Inc.</copyright_cline>
       {% if DYNAMIC_PRESET_DATA['descriptiveMetadata']['Description80'] is defined and DYNAMIC_PRESET_DATA['descriptiveMetadata']['Description80'] != ""  %}
       <description>{{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['Description80']}}</description>
       {% endif %}
       <genre>{{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['Genre'] | default("")}}</genre>
       <series_name>{{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['Series Title'] | default("")}}</series_name>
       <season_title>Season {{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['Season Number'] | default("") }}</season_title>
       {% if DYNAMIC_PRESET_DATA['descriptiveMetadata']['Series Description80'] is defined and DYNAMIC_PRESET_DATA['descriptiveMetadata']['Series Description80'] != ""  %}
       <season_description>{{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['Series Description80']}}</season_description>
       {% elif DYNAMIC_PRESET_DATA['descriptiveMetadata']['AssetSeriesDescription80'] is defined and DYNAMIC_PRESET_DATA['descriptiveMetadata']['AssetSeriesDescription80'] != ""  %}
       <season_description>{{ DYNAMIC_PRESET_DATA['descriptiveMetadata']['AssetSeriesDescription80']}}</season_description>
       {% endif %}
       <accessibility_info>
           <accessibility available="true" role="captions"/>
       </accessibility_info>
           {% set video_file_size = 0 %}
      {% set cc_checksum = "" %}
      {% set cc_file_size = 0 %}
      <assets>
         <asset type="full">
      {%for assets in DYNAMIC_PRESET_DATA['assets_info_deliverables']%}
            {% if assets['asset_type'] == "video" %}
               {% set video_checksum = assets['asset_checksum'] | default("") %}
               {% set video_file_size = assets['asset_fileSize'] | default("") %}
                <data_file role="source">
                   <locale name="en-US"/>
                   <file_name> {{ assets['delivery_file_name'] | default("") }}</file_name>
                   <duration>{{ adjusted }}</duration>
                   <size>{{ video_file_size }}</size>
                   <checksum type="md5">{{ video_checksum }}</checksum>
                </data_file>
            {% elif assets['asset_type'] == "caption" %}
               {% set cc_checksum = assets['asset_checksum'] | default("") %}
               {% set cc_file_size = assets['asset_fileSize'] | default("") %}
                <data_file role="captions">
                    <file_name> {{ assets['delivery_file_name'] | default("") }}</file_name>
                     <size>{{ cc_file_size }}</size>
                    <checksum type="md5">{{ cc_checksum }}</checksum>
                </data_file>
            {%endif%}
       {% endfor %}
          </asset>
        </assets>
       <preview starttime="{{ DYNAMIC_PRESET_DATA['calculatedData']['firstSegmentTRTInSecs']}}"/>
       <products>
           <product>
               <territory>US</territory>
               <sales_start_date>{{DYNAMIC_PRESET_DATA['descriptiveMetadata']['activationDate'] | default("")}}</sales_start_date>
               <cleared_for_sale>true</cleared_for_sale>
           </product>
       </products>
   </video>
</package>