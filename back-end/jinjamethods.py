from jinja2 import exceptions
from jinja2.exceptions import UndefinedError
import json
import os
import re
import lxml.etree as ET
from yattag import indent
from json.decoder import JSONDecodeError


def check_valid_template(jinja2_env, json_request):
    error = None
    try:
        jinja2_tpl = jinja2_env.from_string(
            json_request['request_info']['template'])

        with open('back-end/output/jinja.tpl', "w") as file:
            file.write(str(json_request['request_info']['template']))
    except (exceptions.TemplateSyntaxError, exceptions.TemplateError) as e:
        if isinstance(str(e), str):
            error = "[!] Syntax error in jinja2 template: {0} in line No {1} ".format(
                str(e), str(e.lineno))
    return error


def check_valid_json(jinja2_env, json_request):
    error = None
    json_data = None
    if json_request['request_info']['input_type'] == "json":
        json_datas = json_request['request_info']['dynamic']
        try:
            json_data = json_datas.encode().decode('unicode-escape')
            json_data = json.loads(json_data)
        except (ValueError, json.decoder.JSONDecodeError) as e:
            if isinstance(str(e), str):
                error = "[!] You have not put valid json in DYNAMIC_PRESET_DATA box - {0}".format(
                    str(e))
    return error, json_data


def checking_calculatedData(json_data):
    if "calculatedData" not in json_data:
        data = {
            "calculatedData": None
        }
        json_data.update(data)
    json_data = json.dumps(json_data)
    return json_data


def checking_condition(id):
    is_condition = False
    id = id-1
    with open('back-end/output/jinja.tpl', "r") as file2:
        for idx, lines in enumerate(file2):
            if idx == id:
                if "if" in lines and "defined" in lines and ("length" in lines or '!=' in lines):
                    is_condition = True
                    break
    return is_condition


def set_functionality(line):
    result1 = re.findall('\\b'+"set"+'\\b', line)
    check_set_data = False
    set_name = ""
    if len(result1) > 0:
        if "{%set" in line:
            set_name = line.split()[1].rstrip("=")
        else:
            set_name = line.split()[1].rstrip("=")
        if "{%" in line and "DYNAMIC_PRESET_DATA" in line and "{%" in line:
            check_set_data = True
    return set_name, check_set_data


def warning_data(id, replace_data, line, dupl_set_names, check_set_data=False):
    is_condition = checking_condition(id)
    if 'default' not in replace_data and not is_condition:
        for set_val in dupl_set_names:
            if check_set_data:
                break
            replace_data1 = re.findall('\\b'+set_val+'\\b', replace_data)
            if len(replace_data1) > 0:
                break
        else:
            data = "Line " + str(id+1) + '$$$###' + " - " + line
            return data


def find_empty_tags(value, line):
    value = json.loads(value)
    bracket_values = re.findall(r"[^[]*\[([^]]*)\]", line)
    tag_values = re.findall('<(.+?)>', line)
    final_val = None
    empty_val = None
    if bracket_values and "DYNAMIC_PRESET_DATA" in line:
        for idx, val in enumerate(bracket_values):
            if idx == 0 or val == "'descriptiveMetadata'" or val == "'calculatedData'":
                final_val = value.get(val.replace("'", ''))
            elif final_val is not None:
                if val.isdigit() or ':' in val:
                    val = val.replace("'", '')
                    split_data = val.split(":")
                    if len(split_data) == 2:
                        final_val = final_val[int(
                            split_data[0]):int(split_data[1])]
                    elif len(split_data) == 3:
                        final_val = final_val[int(split_data[0]):int(
                            split_data[1]):int(split_data[2])]
                    else:
                        final_val = final_val[int(split_data[0])]
                else:
                    final_val = final_val.get(val.replace("'", ''))
                if final_val is None:
                    empty_val = val
                    break
    if empty_val is not None:
        if len(tag_values) > 0:
            empty_val = "<" + \
                str(tag_values[0]) + \
                "> tag is empty expecting input from " + "$$$#" + empty_val
    return empty_val


def manipulating_jinja(jinja2_env, json_request, value):
    xml_valid = False
    warning = []
    json_data = value
    empty_tags = []
    set_empty = []
    default_warning = []
    set_warning = []
    has_warning = False
    has_empty_tags = False
    set_names = []
    with open('back-end/output/jinja.tpl', "r") as file:
        for id, line in enumerate(file):
            set_name, check_set_data = set_functionality(line)
            set_names.append(set_name)
            dupl_set_names = set(set_names)
            if None in dupl_set_names:
                dupl_set_names.remove(None)
            elif "" in dupl_set_names:
                dupl_set_names.remove('')
            if check_set_data:
                warnings = warning_data(id, replace_data=line, line=line,
                                        dupl_set_names=dupl_set_names, check_set_data=check_set_data)
                default_warning.append(warnings)
            if "xml" in line:
                xml_valid = True
            if xml_valid:
                result = re.findall('\\b'+"set"+'\\b', line)
                if len(result) > 0:
                    data = "Line " + str(id+1) + "$$$###" + " - " + line
                    set_warning.append(data)
                if "{%" not in line:
                    if "{{" in line and "}}" in line and "xml" not in line:
                        replace_data = line[line.index(
                            "{{"):line.index("}}")+2]
                        warnings = warning_data(
                            id, replace_data, line, dupl_set_names)
                        default_warning.append(warnings)
                        empty_value = find_empty_tags(json_data, line)
                        empty_tags.append(empty_value)
    if default_warning:
        default_warn = {}
        default_warnings = set(default_warning)
        if None in default_warnings:
            default_warnings.remove(None)
        if list(default_warnings):

            default_warnings = list(default_warnings)
            default_warnings.sort()
            default_warn['warning_header'] = "Kindly add default/if conditions for below line no's and code"
            default_warn['warning_message'] = default_warnings
            warning.append(default_warn)
    if set_warning:
        set_warn = {}
        set_warnings = set(set_warning)
        if None in set_warnings:
            set_warnings.remove(None)
        if list(default_warnings):
            set_warnings = list(set_warnings)
            set_warnings.sort()
            set_warn['warning_header'] = "Set variable is used after xml declarations in below line no's and code"
            set_warn['warning_message'] = set_warnings
            warning.append(set_warn)
    if warning:
        has_warning = True

    if empty_tags:
        set_empty = set(empty_tags)

        if None in set_empty:
            set_empty.remove(None)

    if set_empty:
        has_empty_tags = True
    return warning, has_warning, list(set_empty), has_empty_tags


def success_data(jinja2_env, json_request, json_data):
    sub_dynamic = "{\"DYNAMIC_PRESET_DATA\":"
    val = checking_calculatedData(json_data)
    value = sub_dynamic + val + "}"
    value = json.loads(value)
    jinja2_tpl = jinja2_env.from_string(
        json_request['request_info']['template'])
    warning, has_warning, empty_tags, has_empty_tags = manipulating_jinja(
        jinja2_env, json_request, val)
    try:
        rendered_xml = jinja2_tpl.render(value)
        rendered_xml = indent(rendered_xml)
        result = {
            'status_code': 200,
            'template_output': rendered_xml,
            'has_warning': has_warning,
            'warnings': warning,
            'has_empty_tags': has_empty_tags,
            'empty_tags': empty_tags
        }
        return result
    except Exception as e:
        error = "Problem While rendering xml {0} .".format(str(e))
        result = {
            'message': error
        }
        return result, 400
